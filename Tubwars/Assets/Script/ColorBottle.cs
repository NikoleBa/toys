using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ColorBottle : MonoBehaviour
{
    public static event EventHandler OnChangeColor;
    public Animator animator;
    public AudioSource drip;
    
    void Start()
    {
        
    }


    void Update()
    {
        
    }

    public void OnBottlePress(AnimationEvent e)
    {
        drip.Play();
        OnChangeColor?.Invoke(this, EventArgs.Empty);
    }

}
