using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio : MonoBehaviour
{

    public AudioSource[] music;
    private int index = 0;


    public void RadioOn()
    {
        for (int i = music.Length - 1; i >= 0; i--)
        {
            music[i].Stop();
        }
        music[index].Play();
        index++;

        if(index >= music.Length)
        {
            index = 0;
        }
    }

           

}

