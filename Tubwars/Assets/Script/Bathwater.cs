using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Bathwater : MonoBehaviour

{

    public Color32 pink;
    public Color32 green;
    public Color32 blue;
    public Color32 black;
    public Color32 purple;
    public Color32 orange;
    private Color32 [] bathwaterColor;
    public SpriteRenderer bathwater;
    private int colorIndex =0;


    private void Awake()
    {
        ColorBottle.OnChangeColor += ChangeColor;   
    }

    void Start()
    {
        bathwaterColor = new Color32[6];
        bathwaterColor[0] = pink;
        bathwaterColor[1] = green;
        bathwaterColor[2] = blue;
        bathwaterColor[3] = black;
        bathwaterColor[4] = purple;
        bathwaterColor[5] = orange;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ChangeColor(object sender, EventArgs e)
    {
        bathwater.color = bathwaterColor[colorIndex];
        colorIndex++;

        if(colorIndex >=bathwaterColor.Length)
        {
            colorIndex = 0;
        }

    }
}
