using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class NessieSpawner : MonoBehaviour , ISpawner
{
    public GameObject[] prefab;
    public GameObject toys;
    public AudioSource splash;

    private void Start()
    {

    }

    public void SendToy()
    {
        GameObject nessie = GameObject.Instantiate(prefab[Random.Range(0, prefab.Length)],
            this.transform.position, Quaternion.identity, toys.transform);
        nessie.GetComponent<ToyNessie>().startposition = this.transform.position;
        splash.Play();
    }
}
