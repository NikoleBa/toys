using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private float timer = 0;
    public GameObject prefab, ducks;

    public bool enemySpawner = false;

    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= 3)
        {
            GameObject duck = GameObject.Instantiate(prefab, this.transform.position, Quaternion.identity, ducks.transform);
            duck.GetComponent<Toy>().startposition = this.transform.position;


            if (enemySpawner)
            {
                duck.GetComponent<Toy>().direction *= -1;
                duck.GetComponent<SpriteRenderer>().flipX = false;
            }

            timer = 0;

        }
    }
}
