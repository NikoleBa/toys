using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BathBomb : MonoBehaviour
{

    public GameObject toyParent;
    public Animator animator;
    public AudioSource bigSplash;



    public void Splash(AnimationEvent e)
    {
        for (int i = toyParent.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(toyParent.transform.GetChild(i).gameObject);
        }
    }

    public void ThrowBomb()
    {
        animator.SetTrigger("Boom");
    }

    public void playSplash()
    {
        bigSplash.Play();
    }
}
