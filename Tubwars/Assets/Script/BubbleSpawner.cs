using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class BubbleSpawner : MonoBehaviour
{
    public GameObject [] bubbles;
    private float timer = 2f;



    private void Start()
    {
        RandomTimer();
    }

    public void SendBubble()
    {

       GameObject bubble = GameObject.Instantiate(bubbles[Random.Range(0, bubbles.Length)], this.transform.position, Quaternion.identity,transform);
        GameObject.Destroy(bubble, 8);
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        if(timer <=0)
        {
            SendBubble();

            switch (Soap.soapStateIndex)
            {
                case 0:
                    TimerLong();
                        break;

                case 1:
                    TimerMedium();
                        break;
                case 2:
                    TimerFast();
                    break;
            }
        }


    }

    private void RandomTimer()
    {
        this.timer = Random.Range(2, 8);
    }

    private void TimerFast()
    {
        this.timer = Random.Range(1, 2);
    }

    private void TimerMedium()
    {
        this.timer = Random.Range(2, 4);
    }

    private void TimerLong()
    {
        this.timer = Random.Range(7, 12);
    }
}