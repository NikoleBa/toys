using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour
{
    public FixedJoint2D joint;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Connect" + collision);
        if(CompareTag("Duck"))
        {
            joint.enabled=true;
            joint.connectedBody = collision.GetComponent<Rigidbody2D>();
        }
    }
}
