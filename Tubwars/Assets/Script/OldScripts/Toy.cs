using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Toy : MonoBehaviour
{
    public float speed = 300f;
    public int health = 3;
    public int damage = 1;
    public int soap = 1;
    public Vector3 startposition;
    public bool isEnemy = true;
    private Player player;
    public Vector3 direction = new Vector3(1, 0, 0);

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }
    void Update()
    {
        this.transform.position = this.transform.position + direction *speed * Time.deltaTime;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Unit") && !isEnemy)
        {
            Attack(collision.GetComponent<Toy>());
        }
    }

    private void Attack(Toy enemy)
    {
        enemy.TakeDmg(damage);
    }

    private void TakeDmg(int dmg)
    {
        health -= dmg;

        if(health <=0)
        {
            ToyGiveSoap();
            Die();
        }
    }

    private void Die()
    {
        Destroy(this);
    }


    private void ToyGiveSoap()
    {
        player.GiveSoap(soap);
    }


}
