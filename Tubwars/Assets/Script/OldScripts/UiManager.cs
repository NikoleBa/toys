using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class UiManager : MonoBehaviour
{
    public TextMeshProUGUI soapLabel;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void UpdateSoapLable(int soap)
    {
        soapLabel.text=soap.ToString();
    }
}
