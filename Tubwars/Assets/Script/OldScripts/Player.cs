using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int soap = 0;
    public UiManager uiManager;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void GiveSoap(int soap)
    {
        this.soap += soap;
        uiManager.UpdateSoapLable(this.soap);
    }
}
