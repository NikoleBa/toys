using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToyButton : MonoBehaviour
{

    public GameObject spawner;
    public Transform toyHolder;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnButtonPress()
    {
        if(toyHolder.childCount >= 6)
        {
            return;
        }

        else
        {
            spawner.GetComponent<ISpawner>().SendToy();
        }
    }
}
