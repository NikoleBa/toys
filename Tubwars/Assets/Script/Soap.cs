using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soap : MonoBehaviour
{
    public static int soapStateIndex = 0;
    public GameObject [] soapStates;
    public GameObject [] bubbleSpawner;
    public AudioSource soap;


    void Start()
    {
        soapStateIndex= 0;
    }

    void Update()
    {
        
    }

    public void MoreSoap()
    {

        soap.Play();

        if (soapStateIndex >= 2)
        {
            soapStateIndex = 0;
        }
        else
        {
            soapStateIndex++;
        }
   

        switch (Soap.soapStateIndex)
        {
            case 0:
                soapStates[0].SetActive(true);
                soapStates[1].SetActive(false);
                soapStates[2].SetActive(false);
                break;

            case 1:
                soapStates[0].SetActive(false);
                soapStates[1].SetActive(true);
                soapStates[2].SetActive(false);
                break;
            case 2:
                soapStates[0].SetActive(false);
                soapStates[1].SetActive(false);
                soapStates[2].SetActive(true);
                break;
        }
    }
}
