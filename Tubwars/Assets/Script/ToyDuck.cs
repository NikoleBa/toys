using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToyDuck : MonoBehaviour
{
    public AudioSource squeak;
    public AudioSource quack;
    public AudioSource angry;
    public Animator animator;
    public Vector3 startposition;
    private Vector3 direction;
    private float speed = 3f;
    private float oldspeed;


    private float timer = 2f;
    private bool isCollisionBlocked = false;

    private void Start()
    {
        this.transform.position = this.startposition;
        direction = new Vector3(Random.Range(-1f,1f), Random.Range(-1f, 1f), 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (isCollisionBlocked)
        {
         timer -= Time.deltaTime;
            if(timer <=0)
            {
                isCollisionBlocked = false;
            }
        }

        Movement();

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);

            if (hit.collider != null && hit.collider.gameObject == this.gameObject)
            {

                squeak.Play();
                animator.SetTrigger("Click");
            }



        }

        else
        {
            animator.SetTrigger("NoClick");
        }



    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("WallX"))
        {

            direction = new Vector3(direction.x * -1, direction.y, direction.z);


        }

        else if (collision.gameObject.CompareTag("WallY"))
        {

            direction = new Vector3(direction.x, direction.y *-1, direction.z);


        }

        else if ((collision.gameObject.CompareTag("Boat") && !isCollisionBlocked))
        {
            direction = new Vector3(direction.x * -1, direction.y, direction.z);
            isCollisionBlocked = true;
            timer = 2f;

        }

        else if (collision.gameObject.CompareTag("Duck") && !isCollisionBlocked)
        {
            quack.Play();
            animator.SetTrigger("DuckHello");
            direction = new Vector3(direction.x * -1, direction.y, direction.z);
            isCollisionBlocked = true;
            timer = 2f;

        }

        else if (collision.gameObject.CompareTag("Nessie") && !isCollisionBlocked)
        {
            angry.Play();
            animator.SetTrigger("DuckwNessie");
            direction = new Vector3(direction.x * -1, direction.y, direction.z);
            isCollisionBlocked = true;
            timer = 2f;

        }
    }

    private void Movement()
    {

        this.transform.position = this.transform.position + direction * speed * Time.deltaTime;

        if (direction.x>=0)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
            
        }

        else
        {
            this.transform.localScale = new Vector3(1, 1, 1);
        }


    }

    public void Chill(AnimationEvent e)
    {
        oldspeed = speed;
        speed = 0;
    }

    public void UnChill(AnimationEvent e)
    {
        speed = oldspeed;
    }
}
