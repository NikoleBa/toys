using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class DuckSpawner : MonoBehaviour , ISpawner
{
    public GameObject [] prefab;
    public GameObject toys;
    public AudioSource splash;

    private void Start()
    {

    }

    public void SendToy()
    {
        GameObject duck = GameObject.Instantiate(prefab[Random.Range(0,prefab.Length)],
            this.transform.position, Quaternion.identity, toys.transform);
        duck.GetComponent<ToyDuck>().startposition = this.transform.position;
        splash.Play();
    }
}
